(ns advent-2023-clj.day03
  (:require [clojure.string :as str]))

(def input "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..")

(defn- is-symbol-at? [lines range]
  (re-find #"[^\d\.]" (str (get-in lines range))))

(defn- extract-digits [line]
  (->> line
       (re-seq #".*?(\d+)")
       (reduce (fn [{:keys [start-col results]} [full-match number-part]]
                (let [end-col (+ start-col (count full-match))
                      start-col (- end-col (count number-part))]
                  {:start-col end-col
                   :results (conj results
                                  {:number (Long/parseLong number-part)
                                   :column-range [start-col end-col]})}))
          {:start-col 0 :results []})
       :results))

(defn- find-ranges-to-consider [row-size col-size {:keys [column-range row]}]
  (let [[start-col end-col] column-range
        start-col (dec start-col)
        cols-to-consider (range start-col (inc end-col))
        range-to-consider (for [col cols-to-consider
                                row [(dec row) (inc row)]]
                            [row col])
        range-to-consider (conj range-to-consider
                                [row start-col]
                                [row end-col])]
    (filter (fn [[row col]]
              (and (< -1 row row-size)
                   (< -1 col col-size)))
            range-to-consider)))

(defn- is-part-number? [row-size col-size lines {:keys [column-range row] :as c}]
  (let [final-range (find-ranges-to-consider row-size col-size c)]
    (some #(is-symbol-at? lines %) final-range)))

(defn first-part [input]
  (let [lines (str/split-lines input)
        all-digits (mapcat (fn [row line]
                             (map #(assoc % :row row) (extract-digits line)))
                           (range)
                           lines)
        row-size (count lines)
        col-size (-> lines first count)]
    (->> all-digits
         (filter #(is-part-number? row-size col-size lines %))
         (map :number)
         (reduce +))))


(defn- is-gear-at? [lines range]
  (= "*" (str (get-in lines range))))

(defn second-part [input]
  (let [lines (str/split-lines input)
        row-size (count lines)
        col-size (-> lines first count)
        all-digits (mapcat (fn [row line]
                             (map #(assoc % :row row) (extract-digits line)))
                           (range)
                           lines)
        with-gears-ranges (for [digit all-digits
                                :let [ranges (find-ranges-to-consider row-size col-size digit)
                                      ranges-with-gears (filter #(is-gear-at? lines %) ranges)]
                                range ranges-with-gears]
                            (assoc digit :gear-range range))
        grouped (group-by :gear-range with-gears-ranges)]
    (->> grouped
         (filter (fn [[_ vals]] (-> vals count (= 2))))
         (map (fn [[_ [d1 d2]]] (* (:number d1) (:number d2))))
         (reduce +))))
