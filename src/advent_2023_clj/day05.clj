(ns advent-2023-clj.day05
  (:require [clojure.string :as str]))

(def input "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4")

(defn numbers [input]
  (map #(Long/parseLong %) (re-seq #"\d+" input)))

(defn- froms-tos [input]
  (->> input
       (re-seq #"(?s)map:\n(.*?)(?:\n\n|$)")
       (map (fn [[_ match]]
              (->> match
                   str/split-lines
                   (map #(let [[to from size] (numbers %)]
                           {:from [from (+ from size -1)]
                            :to to})))))))

(defn- translate [thing ranges]
  (let [some-range (some (fn [{:keys [from to]}]
                           (when (<= (first from) thing (second from))
                             (+ to (- thing (first from)))))
                         ranges)]
    (or some-range thing)))

(defn first-part [input]
  (let [seeds (numbers (re-find #"seeds:.*" input))
        conversions (froms-tos input)
        soils (reduce (fn [seeds conversion]
                        (map #(translate % conversion) seeds))
                      seeds
                      conversions)]
    (apply min soils)))

(defn translate-range-of-things [thing conversion]
  (let [[start-thing end-thing] thing
        {:keys [from to]} conversion
        [start-range end-range] from
        translated-start (- start-thing start-range)
        translated-end (- end-thing start-range)

        start-in-range? (<= start-range start-thing end-range)
        end-in-range? (<= start-range end-thing end-range)
        both-in-range? (and start-in-range? end-in-range?)]

    (cond
      both-in-range?
      {:news [[(+ to translated-start) (+ to translated-end)]]}

      start-in-range?
      {:news [[(+ to translated-start) (+ (- end-range start-range) to)]]
       :olds [[(inc end-range) end-thing]]}

      end-in-range?
      {:news [[to (+ to translated-end)]]
       :olds [[start-thing (dec start-range)]]}

      :nothing-in-range
      {:olds [thing]})))

(defn- translate-ranges [seeds conversion]
  (let [res (reduce (fn [{:keys [olds news]} fragment]
                      (let [results (map #(translate-range-of-things % fragment)
                                         olds)]
                        {:olds (mapcat :olds results)
                         :news (concat news (mapcat :news results))}))
                    {:olds seeds}
                    conversion)]
    (concat (:olds res) (:news res))))

(defn second-part [input]
  (let [seeds (->> input
                   (re-find #"seeds:.*")
                   numbers
                   (partition 2)
                   (map (fn [[first size]] [first (+ first size -1)])))
        conversions (froms-tos input)
        soils (reduce (fn [seeds conversion]
                        (translate-ranges seeds conversion))
                      seeds
                      conversions)]
    (->> soils
         (map first)
         (apply min))))
