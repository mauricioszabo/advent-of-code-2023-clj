(ns advent-2023-clj.day06
  (:require [clojure.string :as str]))

(def input "Time:      7  15   30
Distance:  9  40  200")

(defn- numbers [input]
  (map #(Long/parseLong %) (re-seq #"\d+" input)))

(defn first-part [input]
  (let [[times distances] (->> input str/split-lines (map numbers))
        wins (map (fn [time record-distance]
                    (->> (range (dec time))
                         (map (fn [ms-to-hold]
                                (* ms-to-hold (- time ms-to-hold))))
                         (filter #(> % record-distance))
                         count))
                  times
                  distances)]
    (apply * wins)))

(defn second-part [input]
  (-> input (str/replace #"[ ]" "") first-part))
