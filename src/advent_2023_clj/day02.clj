(ns advent-2023-clj.day02
  (:require [clojure.string :as str]))

(defn- split-colors [subset]
  (let [[_ red] (re-find #"(\d+)\s+red" subset)
        [_ green] (re-find #"(\d+)\s+green" subset)
        [_ blue] (re-find #"(\d+)\s+blue" subset)]
    {:red (or (some-> red Long/parseLong) 0)
     :green (or (some-> green Long/parseLong) 0)
     :blue (or (some-> blue Long/parseLong) 0)}))

(defn- split-game [row]
  (let [[_ game-id] (re-find #"Game (\d+):" row)
        subsets (-> row (str/split #";") (->> (map split-colors)))]
    {:game-id (Long/parseLong game-id)
     :colors subsets}))

(def ^:private limits {:red 12
                       :green 13
                       :blue 14})

(defn- is-possible? [colors]
  (every? (fn [{:keys [red green blue]}]
            (and (<= red (:red limits))
                 (<= green (:green limits))
                 (<= blue (:blue limits))))
          colors))

(defn- parsed-games [input]
  (->> input
       str/split-lines
       (map split-game)))

(defn part-01 [input]
  (->> input
       parsed-games
       (filter (fn [{:keys [colors]}]
                 (is-possible? colors)))
       (map :game-id)
       (reduce +)))

(defn- max-colors [colors]
  (reduce (fn [acc {:keys [red green blue]}]
            (-> acc
                (update :red max red)
                (update :green max green)
                (update :blue max blue)))
          {:red 0 :blue 0 :green 0}
          colors))

(defn part-02 [input]
  (->> input
       parsed-games
       (map (fn [{:keys [colors]}]
              (->> colors max-colors vals (apply *))))
       (reduce +)))
