(ns advent-2023-clj.day07
  (:require [clojure.string :as str]))

(def input "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483")

(def ranks (zipmap ["A" "K" "Q" "J" "T" "9" "8" "7" "6" "5" "4" "3" "2"]
                   (range 13 -1 -1)))

(defn- normalize-cards [row rank-fn ranks]
  (let [[f s] (str/split row #" ")
        card-vals (mapv ranks (str/split f #""))]
    {:cards (str/split f #"")
     :score [(-> card-vals rank-fn vec (conj 0 0 0 0)  (subvec 0 5))
             card-vals]
     :bid (Long/parseLong s)}))

(defn- calculate-score [input rank-fn ranks]
  (->> input
       str/split-lines
       (map #(normalize-cards % rank-fn ranks))
       (sort-by :score)
       (map (fn [index {:keys [bid]}] (* bid (inc index))) (range))
       (reduce +)))

(defn first-part [input]
  (calculate-score input #(-> % frequencies vals sort reverse)
                   ranks))

(def ranks-part-2
  (zipmap ["A" "K" "Q" "T" "9" "8" "7" "6" "5" "4" "3" "2" "J"]
          (range 13 -1 -1)))

(defn second-part [input]
  (calculate-score input
                   (fn [card-vals]
                     (let [joker? #(= 1 %)
                           non-jokers (remove joker? card-vals)
                           joker-num (- 5 (count non-jokers))
                           [f & r] (-> non-jokers frequencies vals sort reverse)]
                       (cons (+ joker-num (or f 0)) r)))
                   ranks-part-2))
