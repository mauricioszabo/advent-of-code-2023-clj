(ns advent-2023-clj.day01
  (:require [clojure.string :as str]))

(def conversions {"zero" 0
                  "one" 1
                  "two" 2
                  "three" 3
                  "four" 4
                  "five" 5
                  "six" 6
                  "seven" 7
                  "eight" 8
                  "nine" 9})
(let [literals (->> conversions keys (str/join "|"))]
  (def number-regex-part (str "\\d|" literals)))

(defn- to-digit [digit-or-literal]
  (let [found (conversions digit-or-literal)]
    (or found (Long/parseLong digit-or-literal))))

(defn- real-digit [line]
  (let [two-digits (re-find (re-pattern (str "("
                                             number-regex-part
                                             ").*("
                                             number-regex-part
                                             ")"))
                            line)]
    (if two-digits
      (subvec two-digits 1)
      (repeat 2 (re-find (re-pattern number-regex-part) line)))))

(defn- parts [input find-digits]
  (->> input
       str/split-lines
       (map find-digits)
       (reduce (fn [acc [f s]]
                 (+ acc (Long/parseLong (str (to-digit f) (to-digit s)))))
               0)))

(defn first-part [input]
  (parts input #(or (some-> (re-find #"(\d).*(\d)" %) (subvec 1))
                    (vec (repeat 2 (re-find #"\d" %))))))

(defn second-part [input]
  (parts input real-digit))
