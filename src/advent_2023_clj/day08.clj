(ns advent-2023-clj.day08
  (:require [clojure.string :as str]))

(def input "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)")

(defn- sanitize-input [input]
  (let [[_ instructions] (re-find #"(.*)\n\n" input)
        instructions (cycle (str/split instructions #""))
        positions (->> input
                       str/split-lines
                       (drop 2)
                       (map (fn [row]
                              (let [[f s] (str/split row #" = ")
                                    [left right] (re-seq #"[A-Z\d]+" s)]
                                [f {"L" left "R" right}])))
                       (into {}))]
    {:positions positions
     :instructions instructions}))

(defn first-part [input]
  (let [{:keys [positions instructions]} (sanitize-input input)]
    (->> instructions
         (reductions (fn [current movement]
                       (get-in positions [current movement]))
                 "AAA")
         (take-while #(not= "ZZZ" %))
         count)))

(defn- gdc [a b]
  (if (zero? b)
    a
    (recur b (rem a b))))

(defn- lcm [a b]
  (* b (/ a (gdc a b))))

(defn second-part [input]
  (let [{:keys [positions instructions]} (sanitize-input input)
        start-positions (re-seq #"(?s)[A-Z\d]{2}A" input)
        all-results (for [start start-positions]
                      (->> instructions
                           (reductions (fn [current movement]
                                         (get-in positions [current movement]))
                                       start)
                           (take-while #(not (str/ends-with? % "Z")))
                           count))]
    (reduce lcm all-results)))
    ; all-results))
