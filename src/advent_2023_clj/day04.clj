(ns advent-2023-clj.day04
  (:require [clojure.string :as str]
            [clojure.set :as set]))

(def input "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11")

(defn first-part [input]
  (let [lines (str/split-lines input)]
    (->> lines
         (map (fn [line]
                (let [norm (str/replace-first line #".*: " "")
                      [win have] (str/split norm #"\|")
                      num-winnings (count
                                    (set/intersection (set (map #(Long/parseLong %) (re-seq #"\d+" win)))
                                                      (set (map #(Long/parseLong %) (re-seq #"\d+" have)))))]
                  (if (zero? num-winnings)
                    0
                    (Math/pow 2 (dec num-winnings))))))
         (reduce +))))

(defn- add-copies [copies new-copies how-many]
  (reduce (fn [acc copy]
            (update acc copy (fnil + 0) how-many))
          copies
          new-copies))

(defn second-part [input]
  (let [lines (str/split-lines input)]
    (->> lines
         (map-indexed (fn [card line]
                        (let [norm (str/replace-first line #".*: " "")
                              [win have] (str/split norm #"\|")
                              num-winnings (count
                                            (set/intersection (set (map #(Long/parseLong %) (re-seq #"\d+" win)))
                                                              (set (map #(Long/parseLong %) (re-seq #"\d+" have)))))]
                          [card (range (inc card) (+ 1 card num-winnings))])))
         (reduce (fn [copies [card new-copies]]
                   (let [new-cards (inc (count new-copies))
                         copies-of-this-card (get copies card)]
                     (add-copies copies new-copies copies-of-this-card)))
                 (into {} (map vector (range (count lines)) (repeat 1))))
         vals
         (reduce +))))
